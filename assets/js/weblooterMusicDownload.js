var WeblooterMusic = {
    /** Признак режима отладки */
    isDebugMode: false,

    init: function () {
        try {
            WeblooterMusic.Console.info('Инициализация запущена.');

            WeblooterMusic.Player.beginTrackChangeListener();

            WeblooterMusic.Console.success('Инициализация прошла успешно.')
        } catch (e) {
            WeblooterMusic.Console.error('Инициализация зкончилась ошибкой: ' + e.message + '.');
        }

    },

    /**
     * Модуль работы с консолью
     */
    Console: {
        /**
         * Вывод в консоль сообщения об успехе
         * @param $strText
         */
        success: function ($strText) {
            console.log('%c' + $strText, 'background: #42b983; color: #2c3e50; border-radius: 3px; padding: 3px 6px;');
        },

        /**
         * Вывод в консоль сообщения с ошибкой
         * @param $strText
         */
        error: function ($strText) {
            console.log('%c' + $strText, 'background: #d12; color: #f3f5f7; border-radius: 3px; padding: 3px 6px;');
        },

        /**
         * Вывод в консоль сообщения с информацией
         * @param $strText
         */
        info: function ($strText) {
            console.log('%c' + $strText, 'background: #2c3e50; color: #f3f5f7; border-radius: 3px; padding: 3px 6px;');
        },
    },

    /**
     * Модуль для работы с плеером
     */
    Player: {
        _instance: null,

        /**
         * Получение объекта плеера
         * @returns {null}
         */
        getInstance: function () {
            if (WeblooterMusic.Player.instance === null) {
                if (typeof Mu.blocks.di.repo.player != "undefined") {
                    WeblooterMusic.Player.instance = Mu.blocks.di.repo.player;
                } else {
                    throw new Error('Не удалось получать объект плеера.');
                }
            }
            return Mu.blocks.di.repo.player;
        },

        /**
         * Получение данных по текущему треку
         * @returns Object|null
         */
        getTrackData: function () {
            return WeblooterMusic.Player.getInstance().getTrack();
        },

        /** Снимо текущего трека */
        currentTrack: null,

        /**
         * Сверяет играющий трек со снимком текущего трека
         * @return void
         */
        compareCurrentTrack: function () {
            var newCurrentTrack = WeblooterMusic.Player.getTrackData();
            if (newCurrentTrack !== null) {
                if (
                    WeblooterMusic.Player.currentTrack === null
                    || (
                        newCurrentTrack.hasOwnProperty('id')
                        && WeblooterMusic.Player.currentTrack.hasOwnProperty('id')
                        && newCurrentTrack.id != WeblooterMusic.Player.currentTrack.id
                    )
                ) {
                    WeblooterMusic.Player.setCurrentTrack(newCurrentTrack);
                }
            }
        },

        /**
         * Задать новый снимок текущего трека
         */
        setCurrentTrack: function ($objTrack) {
            WeblooterMusic.Player.currentTrack = $objTrack;
            WeblooterMusic.Controls.Bar.rebuild();

            if (WeblooterMusic.isDebugMode) {
                WeblooterMusic.Console.info('DebugMode: Обновлен текущий трек.');
            }
        },

        /**
         * Запустить прослушку изменения треков
         */
        beginTrackChangeListener: function () {
            setInterval(function () {
                WeblooterMusic.Player.compareCurrentTrack();
            }, 100);
        }
    },

    /**
     * Модуль работы с визульными блоками
     */
    Controls: {

        /**
         * Модуль блока нижнего бара
         */
        Bar: {
            /** Шаблон кнопки */
            templateButtons: `
<div class="wbl-music-bar__btn-add-layer">
    <span class="wbl-music-bar__btn {{actionType}}" title="{{title}}" onclick="WeblooterMusic.DownloadList.changeCurrentTrackState()"></span>
</div>
<div class="wbl-music-bar__btn-download-layer">
    <span class="wbl-music-bar__btn wbl-music-bar__btn_download" onclick="WeblooterMusic.Controls.DownloadList.toggleState()" title="Скачать выбранные треки"></span>
    <div class="wbl-music-download-list-wrapper {{downloadListOpenState}}"></div>
</div>
`,

            /**
             * Собирает объект HtmlElement по шаблону
             * @returns {HTMLElement}
             */
            buildHtml: function () {
                var $layer = document.createElement('span');
                $layer.classList.add('wbl-music-bar__btns-layer');

                var innerHtml = WeblooterMusic.Controls.Bar.templateButtons;
                innerHtml = innerHtml.replace('{{actionType}}', (WeblooterMusic.DownloadList.isTrackInList() ? 'wbl-music-bar__btn_remove' : 'wbl-music-bar__btn_add'));
                innerHtml = innerHtml.replace('{{title}}', (WeblooterMusic.DownloadList.isTrackInList() ? 'Удалить из списка на скачивание' : 'Добавить в список на скачивание'));
                innerHtml = innerHtml.replace('{{downloadListOpenState}}', (WeblooterMusic.Controls.DownloadList.isOpened() ? 'wbl-music-download-list-wrapper_opened' : ''));
                $layer.innerHTML = innerHtml;
                $layer.querySelector('.wbl-music-download-list-wrapper').appendChild(WeblooterMusic.Controls.DownloadList.buildHtml());
                return $layer;
            },

            /**
             * Пересобирает кнопку добавления трека на скачивание
             */
            rebuild: function () {
                if (document.querySelectorAll('.player-controls').length > 0) {
                    if (WeblooterMusic.isDebugMode) {
                        WeblooterMusic.Console.info('Инициализирована пересборка html бара.');
                    }
                    var htmlBtn = WeblooterMusic.Controls.Bar.buildHtml();
                    if (document.querySelectorAll('.wbl-music-bar__btns-layer').length > 0) {
                        document.querySelector('.wbl-music-bar__btns-layer').remove();
                    }
                    document.querySelector('.player-controls').appendChild(htmlBtn);
                }
            }
        },

        /**
         * Модуль списка треков на скачивание
         */
        DownloadList: {
            templateLayer: `
<div class="wbl-music-download-list__layer">
    <div class="wbl-music-download-list__items-scroll-layer">
        <div class="wbl-music-download-list__items"></div>
    </div>
    <div class="wbl-music-download-list__checkbox-layer">
        <input type="checkbox" name="WEBLOOTER_MUSIC_GROUP_BY_ARTIST" value="Y" class="wbl-music-download-list__checkbox-input" id="weblooter-music-group-by-artist" onchange="WeblooterMusic.Controls.DownloadList.toggleGroupByArtist();" /><label class="wbl-music-download-list__checkbox-label" for="weblooter-music-group-by-artist">Группировать по артистам</label>
    </div>
    <div class="wbl-music-download-list__btns">
        <button type="button" class="wbl-music-download-list__btn wbl-music-download-list__btn_yellow" onclick="WeblooterMusic.Zip.makeArchive(this)">Скачать</button>
        <button type="button" class="wbl-music-download-list__btn wbl-music-download-list__btn_gray" onclick="WeblooterMusic.DownloadList.clearList();">Очистить</button>
    </div>
</div> `,

            templateItem: `
<div class="wbl-music-download-list__name">
    <span class="wbl-music-download-list__artist">{{artist}}</span> - <span class="wbl-music-download-list__title">{{title}}</span>
</div>
<span class="wbl-music-download-list__remove-btn" onclick="{{deleteThisTrack}}"></span>`,
            buildHtml: function () {
                var DownloadList = WeblooterMusic.DownloadList.getList(),
                    $layer = document.createElement('div');
                $layer.classList.add('wbl-music-download-list');

                $layer.innerHTML = WeblooterMusic.Controls.DownloadList.templateLayer;

                if (Object.keys(DownloadList).length > 0) {
                    for (var i in DownloadList) {
                        var $item = document.createElement('div');
                        $item.classList.add('wbl-music-download-list__item');

                        var html = WeblooterMusic.Controls.DownloadList.templateItem;
                        html = html.replace('{{deleteThisTrack}}', 'WeblooterMusic.DownloadList.deleteTrackById(\'' + (DownloadList[i].realId) + '\')');
                        html = html.replace('{{artist}}', DownloadList[i].artist);
                        html = html.replace('{{title}}', DownloadList[i].title);
                        $item.innerHTML = html;

                        $layer.querySelector('.wbl-music-download-list__items').appendChild($item);
                    }

                    $layer.querySelector('.wbl-music-download-list__btn').disabled = false;
                } else {
                    var $item = document.createElement('div');
                    $item.classList.add('wbl-music-download-list__item');
                    $item.innerHTML = WeblooterMusic.Controls.DownloadList.templateItem;
                    $item.querySelector('.wbl-music-download-list__remove-btn').remove();
                    $item.querySelector('.wbl-music-download-list__name').textContent = 'Список скачивания пуст.';
                    $layer.querySelector('.wbl-music-download-list__items').appendChild($item);

                    $layer.querySelector('.wbl-music-download-list__btn').disabled = true;
                }

                $layer.querySelector('#weblooter-music-group-by-artist').checked = WeblooterMusic.Controls.DownloadList._regGroupByArtist;

                return $layer;
            },

            _regIsOpened: false,

            toggleState: function () {
                WeblooterMusic.Controls.DownloadList._regIsOpened = !WeblooterMusic.Controls.DownloadList._regIsOpened;
                WeblooterMusic.Controls.Bar.rebuild();
            },

            isOpened: function () {
                return WeblooterMusic.Controls.DownloadList._regIsOpened;
            },

            _regGroupByArtist: true,

            toggleGroupByArtist: function () {
                WeblooterMusic.Controls.DownloadList._regGroupByArtist = !WeblooterMusic.Controls.DownloadList._regGroupByArtist;
                WeblooterMusic.Controls.Bar.rebuild();
            }

        }
    },

    /**
     * Модуль списка треков на скачивание
     */
    DownloadList: {
        /**
         * Получить список
         * @returns {{}}
         */
        getList: function () {
            if (typeof localStorage.WeblooterMusic != 'undefined' && localStorage.WeblooterMusic != null) {
                return JSON.parse(localStorage.WeblooterMusic);
            } else {
                return {};
            }
        },

        /**
         * Обновить список
         * @param $list
         */
        saveList: function ($list) {
            localStorage.WeblooterMusic = JSON.stringify($list);
        },

        /**
         * Добавить текущий трек
         */
        addCurrentTrack: function () {
            if (WeblooterMusic.Player.currentTrack != null) {
                var $list = WeblooterMusic.DownloadList.getList(),
                    tmpData = {
                        link: '',
                        artist: WeblooterMusic.Player.currentTrack.artists[0]['name'],
                        title: WeblooterMusic.Player.currentTrack.title,
                        realId: WeblooterMusic.Player.currentTrack.realId
                    };

                for (var i in WeblooterMusic.Player.currentTrack) {
                    if (/(storage\.yandex\.[a-z0-9]+\/)/.test(WeblooterMusic.Player.currentTrack[i])) {
                        tmpData.link = WeblooterMusic.Player.currentTrack[i];
                        break;
                    }
                }

                if (tmpData.link !== '') {
                    $list[WeblooterMusic.Player.currentTrack.realId] = tmpData;
                    WeblooterMusic.DownloadList.saveList($list);
                    if (WeblooterMusic.isDebugMode) {
                        WeblooterMusic.Console.success('Текущий трек успешно добавлен.');
                    }
                    WeblooterMusic.Controls.Bar.rebuild();
                } else {
                    if (WeblooterMusic.isDebugMode) {
                        WeblooterMusic.Console.error('Не удалось определить ссылку на файл в треке.');
                    }
                }
            } else {
                if (WeblooterMusic.isDebugMode) {
                    WeblooterMusic.Console.error('Не выбран никакой трек.');
                }
            }
        },

        /**
         * Удалить текущий трек
         */
        deleteCurrentTrack: function () {
            if (WeblooterMusic.Player.currentTrack != null) {
                var $list = WeblooterMusic.DownloadList.getList();
                delete $list[WeblooterMusic.Player.currentTrack.realId];
                WeblooterMusic.DownloadList.saveList($list);
                if (WeblooterMusic.isDebugMode) {
                    WeblooterMusic.Console.success('Текущий трек успешно удален.');
                }
                WeblooterMusic.Controls.Bar.rebuild();
            } else {
                if (WeblooterMusic.isDebugMode) {
                    WeblooterMusic.Console.error('Не выбран никакой трек.');
                }
            }
        },

        /**
         * Удалить конкретный трек
         * @param $intRealId
         */
        deleteTrackById: function ($intRealId) {
            if (WeblooterMusic.Player.currentTrack != null) {
                var $list = WeblooterMusic.DownloadList.getList();
                delete $list[$intRealId];
                WeblooterMusic.DownloadList.saveList($list);
                WeblooterMusic.Controls.Bar.rebuild();
            } else {
                if (WeblooterMusic.isDebugMode) {
                    WeblooterMusic.Console.error('Не выбран никакой трек.');
                }
            }
        },

        /**
         * Изменить состояние текущего трека
         */
        changeCurrentTrackState: function () {
            if (WeblooterMusic.Player.currentTrack != null) {
                if (typeof WeblooterMusic.DownloadList.getList()[WeblooterMusic.Player.currentTrack.realId] !== 'undefined') {
                    WeblooterMusic.DownloadList.deleteCurrentTrack();
                } else {
                    WeblooterMusic.DownloadList.addCurrentTrack();
                }
            } else {
                if (WeblooterMusic.isDebugMode) {
                    WeblooterMusic.Console.error('Не выбран никакой трек.');
                }
            }
        },

        /**
         * Очистить список
         */
        clearList: function () {
            localStorage.WeblooterMusic = JSON.stringify({});
            WeblooterMusic.Controls.Bar.rebuild();
        },

        /**
         * Проверка наличия текущего трека в листе
         * @returns {boolean}
         */
        isTrackInList: function () {
            if (WeblooterMusic.Player.currentTrack != null) {
                return (typeof WeblooterMusic.DownloadList.getList()[WeblooterMusic.Player.currentTrack.realId] !== 'undefined');
            } else {
                if (WeblooterMusic.isDebugMode) {
                    WeblooterMusic.Console.error('Не выбран никакой трек.');
                }
            }
        }
    },

    /**
     * Модуль работы с архивом
     */
    Zip: {
        /** Объект архива */
        _zip: null,

        /**
         * Обновить объект архива на новый
         */
        createNewZip: function () {
            WeblooterMusic.Zip._zip = new JSZip();
            WeblooterMusic.Zip._regFileContinued = 0;
        },

        /**
         * Получить объект архива
         * @returns {null}
         */
        getInstance: function () {
            return WeblooterMusic.Zip._zip;
        },

        /**
         * Запуск формирования массива
         */
        makeArchive: function ($btn) {
            if (WeblooterMusic.isDebugMode) {
                WeblooterMusic.Console.info('Собираем архив.');
            }

            if( !$btn.disabled )
            {
                $btn.disabled = true;
                try
                {
                    $btn.textContent = 'Скачиваем песни...';
                    WeblooterMusic.Zip.createNewZip();
                    var $list = WeblooterMusic.DownloadList.getList();
                    if (Object.keys($list).length > 0) {
                        Object.values($list).reduce(function (Pr, $objTrack) {
                            Pr = Pr.then(function () {
                                try {
                                    WeblooterMusic.Zip.addToArchive($objTrack);
                                } catch (e) {
                                    if (WeblooterMusic.isDebugMode) {
                                        WeblooterMusic.Console.error(e.message);
                                    }
                                }
                            });
                            return Pr;
                        }, Promise.resolve());
                    } else {
                        if (WeblooterMusic.isDebugMode) {
                            WeblooterMusic.Console.error('Список треков на скачивание пуст.')
                        }
                    }
                }
                catch (e) {
                    $btn.disabled = false;
                    if (WeblooterMusic.isDebugMode) {
                        WeblooterMusic.Console.error(e.message);
                    }
                }
            }
            else
            {
                if (WeblooterMusic.isDebugMode) {
                    WeblooterMusic.Console.info('Скачивание не доступно.');
                }
            }

        },

        /**
         * Количество пройденых треков из списка.
         * Увеличивается во время попытки добавить трек в архив
         */
        _regFileContinued: 0,

        /**
         * Запуск попытки добавить трек
         * @param $objTrack
         * @returns {Promise}
         */
        addToArchive: function ($objTrack) {
            return new Promise(function (resolve, reject) {
                JSZipUtils.getBinaryContent($objTrack.link, function (err, data) {
                    ++WeblooterMusic.Zip._regFileContinued;
                    if (err) {
                        if (WeblooterMusic.isDebugMode) {
                            WeblooterMusic.Console.error($objTrack.artist + ' - ' + $objTrack.title + ' / Не удалось добавить в архив.');
                            WeblooterMusic.Console.error(err);
                        }
                    } else {
                        if (WeblooterMusic.Controls.DownloadList._regGroupByArtist) {
                            WeblooterMusic.Zip.getInstance().folder($objTrack.artist).file($objTrack.title + '.mp3', data, {binary: true});
                        } else {
                            WeblooterMusic.Zip.getInstance().file($objTrack.artist + ' - ' + $objTrack.title + '.mp3', data, {binary: true});
                        }
                        if (WeblooterMusic.isDebugMode) {
                            WeblooterMusic.Console.success($objTrack.artist + ' - ' + $objTrack.title + ' / Добавлен в архив.');
                        }
                    }

                    if (Object.keys(WeblooterMusic.DownloadList.getList()).length === WeblooterMusic.Zip._regFileContinued) {
                        WeblooterMusic.Zip.returnZip();
                    } else {
                        if (err) {
                            reject();
                        } else {
                            resolve();
                        }
                    }
                });
            });
        },

        /**
         * Отдает на скачивание сформированный архив
         */
        returnZip: function () {
            if (WeblooterMusic.isDebugMode) {
                WeblooterMusic.Console.success('Отдаем архив.');
            }
            document.querySelector('.wbl-music-download-list__btn').textContent = 'Создаем архив...';
            WeblooterMusic.Zip.getInstance().generateAsync({type: "blob"})
                .then(function (content) {
                    saveAs(content, "Музыкальный архив (Скачано через расширение от Weblooter Inc.).zip");
                    document.querySelector('.wbl-music-download-list__btn').textContent = 'Скачать';
                    document.querySelector('.wbl-music-download-list__btn').disabled = false;
                });
        }
    }
};

WeblooterMusic.init();