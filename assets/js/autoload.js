/*
 Запускается после document.ready на странице
 */

function wblLoadApplication($url) {
    return new Promise(function (resolve, reject) {
        fetch(chrome.extension.getURL($url))
            .then(function (response) {
                if (response.status === 200) {
                    return response.text()
                }
                throw new Error();
            })
            .then(function (response) {
                var $div = document.createElement('script');
                console.log('%c' + $url + ' loaded', 'background: #2c3e50; color: #f3f5f7; border-radius: 3px; padding: 3px 6px;');
                $div.textContent = response;
                document.querySelector('head').appendChild($div);
                resolve({});
            })
            .catch(function (e) {
                console.log('%c'+e.message+' | '+$url, 'background: #d12; color: #f3f5f7; border-radius: 3px; padding: 3px 6px;');
                throw new Error();
            });
    });
}

wblLoadApplication('assets/vendor/FileSaver.js')
    .then(function () {
        wblLoadApplication('assets/vendor/jszip.js').then(function () {
            wblLoadApplication('assets/vendor/jszip-utils.min.js').then(function () {
                wblLoadApplication('assets/js/weblooterMusicDownload.js');
            });
        });
    });